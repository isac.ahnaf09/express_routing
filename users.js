const express = require('express')
const router = express()
const fs = require('fs')

router.get('/user', (req, res) => {
    fs.readFile('./db/users.json', 'utf-8', (err, data) => {
        const dataUser = JSON.parse(data);

        if(err){
            res.status(501).json({
                message : "Server is error",
                errors : err
            })
        }else{
            res.status(200).json({
                message : "this is all data users",
                users : dataUser
            })
        }
    })
})

module.exports = router
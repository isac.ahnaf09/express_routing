const express = require('express')
const app = express()
const fs = require('fs')
const bodyParser = require('body-parser')
const path = require('path')
const cookieParser = require("cookie-parser");
const sessions = require('express-session');
const router = require('./users')
let status = false

//set view engine
app.set('view engine', 'ejs')

//use user.js to get user.json from postman
app.use(router)

//parsing data from html
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }));


// creating maxsession hours from milliseconds
const maxSession = 1000 * 5;

//session middleware
app.use(sessions({
    secret: "thisismysecrctekeyfhrgfgrfrty84fwir767",
    saveUninitialized:true,
    cookie: { maxAge: maxSession },
    resave: false
}));

//serving public file
app.use(express.static(__dirname));

// cookie parser middleware
app.use(cookieParser())

//variable to save a session
let session;


app.get('/', (req, res) => {
    res.sendFile('views/index.html',{root:__dirname})
})

app.get('/login', (req, res) => {
    if (session){
        res.render('notification')
    }else{
        res.render('login')
    }
})

app.post('/login', (req, res) => {
    const { email, password } = req.body 

    fs.readFile('./db/users.json', 'utf-8', (err, data) => {
        const dataUser = JSON.parse(data);

        if(err){
            res.status(501).json({
                message : "Server is error",
                errors : err
            })
        }else{
            if(dataUser[0].email == email && dataUser[0].password == password){
                session = email;
                res.redirect('/')
            }else{
                res.status(404).json({
                    message : "user or password is incorrect"
                })
            }
        }
    })
})

app.get('/logout',(req,res) => {
    req.session.destroy();
    session = null  

    res.redirect('/');
});

app.get('/game', (req, res) => {
    if(session){
        res.sendFile('views/games.html',{root:__dirname})
    }else{
        res.redirect('/login')
    }
})

app.listen(3000, () =>{
    console.log("listening... localhost:3000")
})
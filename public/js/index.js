const dataComOption = ["rock", "paper", "scissors"]
let hasGameStarted = false;

class Game{
    constructor(){
        this.choice = "";
    }

    highlightTheChoice(userChoice, comChoice){
        document.getElementById('user-'+userChoice).classList.add('on-focus');
        document.getElementById('com-'+comChoice).classList.add('on-focus');
    }

    whoIsTheWinner(userChoice, comChoice){
        if(userChoice == comChoice){
          return resultTheWinner = "draw";
        }else{
            if(userChoice == "rock" && comChoice == "paper" || userChoice == "scissors" && comChoice == "rock" || userChoice == "paper" && comChoice == "scissors"){
                return resultTheWinner = "com win";
            }else if(userChoice == "rock" && comChoice == "scissors" || userChoice == "scissors" && comChoice == "paper" || userChoice == "paper" && comChoice == "rock"){
                return resultTheWinner = "user win";
            }else{
                return resultTheWinner = "error";
            }
        }
    }

    whoIsTheWinner(userChoice, comChoice){
        let resultTheWinner;
        if(userChoice == comChoice){
          return resultTheWinner = "draw";
        }else{
            if(userChoice == "rock" && comChoice == "paper" || userChoice == "scissors" && comChoice == "rock" || userChoice == "paper" && comChoice == "scissors"){
                return resultTheWinner = "com win";
            }else if(userChoice == "rock" && comChoice == "scissors" || userChoice == "scissors" && comChoice == "paper" || userChoice == "paper" && comChoice == "rock"){
                return resultTheWinner = "user win";
            }else{
                return resultTheWinner = "error";
            }
        }
    }

    gameResult(theWinnerIs){
        document.getElementById('beforeGameStarted').style.display = 'none';
        
        if(theWinnerIs == 'com win'){
            document.getElementById('com-win').classList.add('result-display')
        }else if(theWinnerIs == 'user win'){
            document.getElementById('user-win').classList.add('result-display')
        }else{
            document.getElementById('draw').classList.add('result-display')
        }
    }

    resetTheGame(){
        //set hasGameStarted == false
        hasGameStarted = false;
    
        let highlightTheChoice = document.getElementsByClassName('on-focus')
        let resetGame = document.getElementsByClassName('result-display')
    
        //menghapus highlight pada user choice dan com choice
        highlightTheChoice[0].classList.remove('on-focus')    
        highlightTheChoice[0].classList.remove('on-focus')
    
        //reset gameResult
        document.getElementById('beforeGameStarted').style.display = 'block';
        resetGame[0].classList.remove('result-display')
        
    }
}

class UserSide extends Game{
    constructor(userPick){
        super()
        this.choice = userPick
    }

    userPick(){
        return this.choice
    }
}   

class ComSide extends Game{
    constructor(){
        super();
    }

    whatComChoice(){
        let comPick = Math.floor(Math.random() * 3);
        const comChoice = dataComOption[comPick]; 
    
        return comChoice;
    }
}

const user = new UserSide;
const com = new ComSide;

function whatUserChoice(userPick){
    let userChoice = userPick;
    let comChoice = com.whatComChoice();
    
    let winner = user.whoIsTheWinner(userChoice, comChoice);

    if(hasGameStarted == false){
        hasGameStarted = true;
        user.highlightTheChoice(userPick,comChoice);
        user.gameResult(winner);
    }
}

function refreshButton(){
    if(hasGameStarted == true){
        user.resetTheGame();
    }
}